<?php

  wp_reset_query();

  global $post;

  // Retrieve the next 5 upcoming events
  $events = tribe_get_events( [ 'posts_per_page' => 4 ] );
  $feature_image = get_the_post_thumbnail_url( get_the_ID() );

?>


 <section class="section home-featured-events">
   <div class="wrap">
     <div class="header">
       <h2>Upcoming Events</h2>
       <a class="button" href="/events/">View All</a>
     </div>
        <div class="row">
        <?php foreach ( $events as $post ) {
           setup_postdata( $post );

           // This time, let's throw in an event-specific
           // template tag to show the date after the title!

           echo '<div class="col-xs-12 col-sm-6 col-lg-3">';
           echo '<a href="' . get_permalink($post) . '">';
           echo '<div class="home-card">';
           echo '<div class="image">' . get_the_post_thumbnail( $post ) . '">';
           echo '<div class="date-box">';
           echo '<p class="date">' . tribe_get_start_date( $post ) . '<p>';
           echo '</div>';
           echo '</div>';
           echo ' <div class="card-footer">';
           echo '<h3>' . $post->post_title . '</h3>';
           echo '<div class="signup-button">' . "Sign Up" . '</div>';
           echo '</div>';
           echo '</div>';
           echo '</a>';
           echo '</div>';
           echo '<a href="' . get_permalink($post) . '">';

        } ?>
        </div>
   </div>
 </section>

 <?php wp_reset_postdata(); ?>

Tribe\Events\Views\V2\Template_Bootstrap;
