<?php
/**
 * Displays the Hero layout
 *
 * Easily convertible into a sideshow by enabling multiple
 * rows in the repeater.
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'homepage_hero' )) : ?>
  <?php while ( have_rows( 'homepage_hero' )) : the_row();
      $hero_heading     = get_sub_field( 'hero_heading' );
      $hero_subheading  = get_sub_field( 'hero_subheading' );
      $hero_link        = get_sub_field( 'hero_link' );
      $hero_background  = get_sub_field( 'hero_background' );
      $hero_background_image = get_sub_field( 'hero_background_image' );
    ?>

    <section class="hero <?php echo $hero_background; ?>">

      <img class="hero-bkg-image" draggable="false" aria-hidden="true"
        src="<?php echo esc_url( $hero_background_image['url'] ); ?>"
        width="<?php echo esc_attr( $hero_background_image['width'] ); ?>"
        height="<?php echo esc_attr( $hero_background_image['height'] ); ?>"
        alt="<?php echo esc_attr( $hero_background_image['alt'] ); ?>">

    </section>

    <div class="hero-box">
      <!-- <a
      href="<?php echo esc_url( $hero_link['url'] ); ?>"
      target="<?php echo esc_attr( $hero_link['target'] ); ?>"> -->
      <div class="hero-content">

        <h1><?php echo $hero_heading; ?></h1>

        <?php if ( ! empty( $hero_subheading )) : ?>
          <p><?php echo $hero_subheading; ?></p>
        <?php endif; ?>

      </div>
    <!-- </a> -->
    </div>
  <?php endwhile; ?>
<?php endif; ?>
