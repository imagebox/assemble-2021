<?php

  $featured_heading = get_field('featured_heading');
  $featured_programs_link = get_field('featured_programs_link');


 ?>

<section class="section home-featured-programs">
  <div class="wrap">
    <div class="header">
      <h2><?php echo $featured_heading; ?></h2>
      <?php if ( $featured_programs_link ) : ?>
        <a class="button"
          href="<?php echo esc_url( $featured_programs_link['url'] ); ?>"
          target="<?php echo esc_attr( $featured_programs_link['target'] ); ?>">
          <?php echo $featured_programs_link['title']; ?>
        </a>
      <?php endif; ?>
    </div>

    <div class="row">
      <?php if ( have_rows( 'home_featured_program' )) : ?>
        <?php while ( have_rows( 'home_featured_program' )) : the_row();

        $featured_thumb_link = get_sub_field('featured_thumb_link');
        $featured_thumb = get_sub_field('featured_thumb');
        $featured_title = get_sub_field('featured_title');
        $featured_date = get_sub_field('featured_date');
        $featured_time = get_sub_field('featured_time');


        ?>
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="home-card">
            <a
              href="<?php echo esc_url( $featured_thumb_link['url'] ); ?>"
              target="<?php echo esc_attr( $featured_thumb_link['target'] ); ?>">
              <div class="thumbnail-box">
                <img draggable="false" aria-hidden="true"
                  src="<?php echo esc_url( $featured_thumb['url'] ); ?>"
                  width="<?php echo esc_attr( $featured_thumb['width'] ); ?>"
                  height="<?php echo esc_attr( $featured_thumb['height'] ); ?>"
                  alt="<?php echo esc_attr( $featured_thumb['alt'] ); ?>">
              </div>
              <div class="thumbnail-footer">
                <div class="content-box">
                  <h3><?php echo $featured_title; ?></h3>
                  <span><?php echo $featured_date; ?></span>
                  <span><?php echo $featured_time; ?></span>
                </div>
              </div>
            </a>
          </div>
        </div>

        <?php endwhile; ?>
      <?php endif; ?>
    </div>


  </div>
</section>
