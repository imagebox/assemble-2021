<?php

  $callout_block_two_header = get_field('callout_block_two_header');
  $callout_block_two_text = get_field('callout_block_two_text');
  $callout_block_two_button = get_field('callout_block_two_button');
  $callout_block_two_button_two = get_field('callout_block_two_button_two');
  $callout_block_two_image = get_field('callout_block_two_image');
  $callout_block_two_image_size = "block_full_width";

 ?>

<section class="section callout-block-two">
  <div class="wrap">
    <div class="block-content">
      <div class="block-photo">
        <img draggable="false" aria-hidden="true"
          src="<?php echo esc_url( $callout_block_two_image['url'] ); ?>"
          width="<?php echo esc_attr( $callout_block_two_image['width'] ); ?>"
          height="<?php echo esc_attr( $callout_block_two_image['height'] ); ?>"
          alt="<?php echo esc_attr( $callout_block_two_image['alt'] ); ?>">
      </div>
      <div class="split-wrap">
        <div class="content-box">
          <h2><?php echo $callout_block_two_header ?></h2>
          <p><?php echo $callout_block_two_text; ?></p>
          <?php if ( $callout_block_two_button ) : ?>
            <a class="button white-button"
            href="<?php echo esc_url( $callout_block_two_button['url'] ); ?>"
            target="<?php echo esc_attr( $callout_block_two_button['target'] ); ?>">
            <?php echo $callout_block_two_button['title']; ?>
          </a>
        <?php endif; ?>
        </div>
        <div class="button-box">
          <?php if ( have_rows( 'homepage_icon_buttons' )) : ?>
            <?php while ( have_rows( 'homepage_icon_buttons' )) : the_row();
              $icon_icon = get_sub_field('icon_icon');
              $icon_button = get_sub_field('icon_button');
            ?>

          <?php if ( $icon_button ) : ?>
              <a
                href="<?php echo esc_url( $icon_button['url'] ); ?>"
                target="<?php echo esc_attr( $icon_button['target'] ); ?>">
                <div class="icon-block">
                   <img src="<?php echo $icon_icon['url']; ?>" alt="<?php echo $icon_icon['alt']; ?>" />
                </div>
                <div class="text">
                  <?php echo $icon_button['title']; ?>
                </div>
              </a>
            <?php endif; ?>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
