<?php

  $callout_block_one_header = get_field('callout_block_one_header');
  $callout_block_one_text = get_field('callout_block_one_text');
  $callout_block_one_button = get_field('callout_block_one_button');
  $callout_block_one_button_two = get_field('callout_block_one_button_two');
  $callout_block_one_image = get_field('callout_block_one_image');
  $callout_block_one_image_size = "block_full_width";

 ?>

<section class="section callout-block-one">
  <div class="wrap">
    <div class="block-content">
      <div class="split-wrap">
        <h2><?php echo $callout_block_one_header ?></h2>
        <p><?php echo $callout_block_one_text; ?></p>
        <div class="button-box">
          <?php if ( $callout_block_one_button ) : ?>
            <a class="button"
              href="<?php echo esc_url( $callout_block_one_button['url'] ); ?>"
              target="<?php echo esc_attr( $callout_block_one_button['target'] ); ?>">
              <?php echo $callout_block_one_button['title']; ?>
            </a>
          <?php endif; ?>
          <?php if ( $callout_block_one_button_two ) : ?>
            <a class="button"
              href="<?php echo esc_url( $callout_block_one_button_two['url'] ); ?>"
              target="<?php echo esc_attr( $callout_block_one_button_two['target'] ); ?>">
              <?php echo $callout_block_one_button_two['title']; ?>
            </a>
          <?php endif; ?>
        </div>
      </div>
    <div class="block-photo">
      <img draggable="false" aria-hidden="true"
        src="<?php echo esc_url( $callout_block_one_image['url'] ); ?>"
        width="<?php echo esc_attr( $callout_block_one_image['width'] ); ?>"
        height="<?php echo esc_attr( $callout_block_one_image['height'] ); ?>"
        alt="<?php echo esc_attr( $callout_block_one_image['alt'] ); ?>">
    </div>
    </div>
  </div>
</section>
