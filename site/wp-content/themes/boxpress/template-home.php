<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-hero' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-featured-programs' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-featured-events' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-callout-block-one' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-callout-block-two' ); ?>



  </article>




<?php get_footer(); ?>
