<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */

  $footer_callout_heading = get_field('footer_callout_heading','option');
  $footer_callout_text = get_field('footer_callout_text','option');
  $footer_callout_link = get_field('footer_callout_link','option');

?>
</main>
<footer id="colophon" class="site-footer section" role="contentinfo">
  <div class="wrap">
   <div class="top-footer">
     <div class="row">
       <div class="col-xs-12 col-lg-3">
         <div class="site-branding">
           <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
             <span class="vh"><?php bloginfo('name'); ?></span>

             <svg class="site-logo" width="200" height="48" focusable="false">
               <use href="#site-logo-footer"/>
             </svg>
           </a>
         </div>
       </div>
       <div class="col-xs-12 col-lg-3">
          <?php get_template_part( 'template-parts/global/address-block' ); ?>
       </div>
       <div class="col-xs-12 col-lg-3">
           <?php get_template_part( 'template-parts/global/social-nav' ); ?>
       </div>
       <div class="col-xs-12 col-lg-3">
         <div class="get-invovled-box">
           <h2><?php echo $footer_callout_heading; ?></h2>
           <p><?php echo $footer_callout_text; ?></p>
           <?php if ( $footer_callout_link ) : ?>
             <a class="button"
               href="<?php echo esc_url( $footer_callout_link['url'] ); ?>"
               target="<?php echo esc_attr( $footer_callout_link['target'] ); ?>">
               <?php echo $footer_callout_link['title']; ?>
             </a>
           <?php endif; ?>
         </div>
       </div>
     </div>
   </div>
   <div class="section bottom-footer">
       <div class="site-copyright">
         <p>
           <small>
             <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
             <?php
               $company_name     = get_bloginfo( 'name', 'display' );
               $alt_company_name = get_field( 'alternative_company_name', 'option' );

               if ( ! empty( $alt_company_name )) {
                 $company_name = $alt_company_name;
               }
             ?>
             <?php echo $company_name; ?>.
             <?php _e('All rights reserved.', 'boxpress'); ?>
           </small>
         </p>
       </div>
       <div class="imagebox">
         <p>
           <small>
             <?php _e('Website by', 'boxpress'); ?>
             <a href="https://imagebox.com" target="_blank">
               <span class="vh">Imagebox</span>
               <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                 <use href="#imagebox-logo"/>
               </svg>
             </a>
           </small>
         </p>
       </div>
   </div>
</footer>
</div>


<footer>




</div>

</footer>


<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
