<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package boxpress
 */

// Prevent 'get_header not a function' error
if ( ! defined( 'ABSPATH' )) exit;

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php'); ?>

  <section class="section blog-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main-col">

          <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part( 'template-parts/content/content', get_post_format() ); ?>

            <?php endwhile; ?>

            <?php boxpress_pagination(); ?>
          <?php else : ?>
            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>
          <?php endif; ?>

        </div>
        <div class="l-aside-col">

          <?php get_sidebar(); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
